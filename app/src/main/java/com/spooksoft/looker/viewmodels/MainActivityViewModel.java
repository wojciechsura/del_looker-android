package com.spooksoft.looker.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.util.Log;

import com.spooksoft.looker.R;
import com.spooksoft.looker.models.Orientation;
import com.spooksoft.looker.models.OrientationF;
import com.spooksoft.looker.services.MessagingService;
import com.spooksoft.looker.services.SensorService;
import com.spooksoft.looker.services.NetClientService;
import com.spooksoft.looker.services.UdpClientService;
import com.spooksoft.looker.viewmodels.interfaces.IMainActivityAccess;

public class MainActivityViewModel extends BaseObservable {
    public ObservableBoolean getUdp() {
        return udp;
    }

    // Private types ----------------------------------------------------------

    private interface OrientationChangedListener {

        void onOrientationChanged(Orientation orientation);
    }

    private class SensorDataGenerator extends AsyncTask<Void, OrientationF, Void> {

        private final OrientationChangedListener orientationChangedListener;

        private SensorDataGenerator(OrientationChangedListener orientationChangedListener) {

            this.orientationChangedListener = orientationChangedListener;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            while (!isCancelled()) {
                OrientationF orientation = sensorService.getOrientation();

                publishProgress(orientation);

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // Thrown if cancelled - break the loop
                    break;
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(OrientationF... values) {
            if (orientationChangedListener != null && values.length > 0) {

                Orientation orientation = new Orientation(
                        (int)(values[0].getYaw() * 180 / Math.PI),
                        (int)(values[0].getPitch() * 180 / Math.PI),
                        (int)(values[0].getRoll() * 180 / Math.PI));

                orientationChangedListener.onOrientationChanged(orientation);
            }
        }
    }

    // Private fields ---------------------------------------------------------

    private IMainActivityAccess access;
    private NetClientService netClient;
    private SensorDataGenerator dataGenerator;
    private final SensorService sensorService;
    private final MessagingService messagingService;
    private final Context context;

    private final ObservableField<String> address;
    private final ObservableField<String> port;
    private final ObservableBoolean connected;
    private final ObservableBoolean udp;

    // Private event handlers -------------------------------------------------

    private OrientationChangedListener orientationChangedListener = new OrientationChangedListener() {
        @Override
        public void onOrientationChanged(Orientation orientation) {

            String data = String.format("%3d|%3d|%3d;", (int)orientation.getPitch(), (int)orientation.getRoll(), (int)orientation.getYaw());
            netClient.send(data);
        }
    };

    private NetClientService.ConnectionListener connectionListener = new NetClientService.ConnectionListener() {

        @Override
        public void success() {
            connected.set(true);
            Log.d("MAVM", "Connected!");

            if (!sensorService.isCollecting())
                sensorService.startCollecting();
            dataGenerator = new SensorDataGenerator(MainActivityViewModel.this.orientationChangedListener);
            dataGenerator.execute((Void)null);
        }

        @Override
        public void failure(String reason) {
            messagingService.inform(context, context.getString(R.string.main_title), String.format(context.getString(R.string.main_cannotConnect), reason));
            connected.set(false);
        }
    };

    // Public methods ---------------------------------------------------------

    public MainActivityViewModel(IMainActivityAccess access, Context context, SensorService sensorService, MessagingService messagingService) {

        this.access = access;
        this.context = context;
        this.sensorService = sensorService;
        this.messagingService = messagingService;

        netClient = new NetClientService();
        dataGenerator = null;
        address = new ObservableField<String>("192.168.43.34");
        port = new ObservableField<String>("10000");
        connected = new ObservableBoolean(false);
        udp = new ObservableBoolean(true);
    }

    public void connect() {

        if (connected.get())
            throw new RuntimeException("Already connected!");

        int port;
        try {

            port = Integer.parseInt(this.port.get());
        } catch (Exception e) {

            messagingService.inform(context, R.string.main_title, R.string.main_invalidPort);
            return;
        }

        netClient.connect(address.get(), port, udp.get() ? NetClientService.ConnectionKind.UDP : NetClientService.ConnectionKind.TCP, connectionListener);
    }

    public void disconnect() {

        if (!connected.get()) {
            throw new RuntimeException("Not connected!");
        }

        dataGenerator.cancel(true);
        dataGenerator = null;
        netClient.disconnect();
        connected.set(false);

        if (sensorService.isCollecting())
            sensorService.stopCollecting();
    }

    // Public properties ------------------------------------------------------

    public ObservableField<String> getAddress() {
        return address;
    }

    public ObservableField<String> getPort() {
        return port;
    }

    public ObservableBoolean getConnected() {
        return connected;
    }
}
