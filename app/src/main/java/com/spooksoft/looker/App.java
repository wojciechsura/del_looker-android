package com.spooksoft.looker;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.spooksoft.looker.services.MessagingService;
import com.spooksoft.looker.services.SensorService;

/**
 * Created by wsura on 19.05.2017.
 */

public class App extends Application {

    private static Context context;
    private static MessagingService messagingService;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
        messagingService = new MessagingService();
    }

    public static SensorService getNewSensorService() {

        return new SensorService(context);
    }

    public static MessagingService getMessagingService() {

        return messagingService;
    }
}
