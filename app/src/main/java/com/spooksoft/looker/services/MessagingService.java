package com.spooksoft.looker.services;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.widget.DatePicker;
import android.widget.TimePicker;

import org.joda.time.DateTime;

/**
 * Created by Wojciech on 2017-04-06.
 */

public class MessagingService {

    public interface AnswerListener {

        void onPositive();
        void onNegative();
    }

    public interface ChoiceListener {

        void onChoice(int which);
    }

    public interface IDateChosenListener {

        void dateChosen(DateTime date);
    }

    public void ask(Context activityContext, @StringRes int title, @StringRes int message, final AnswerListener listener) {

        ask(activityContext, activityContext.getString(title), activityContext.getString(message), listener);
    }

    public void ask(Context activityContext, String title, String message, final AnswerListener listener) {

        ask(activityContext, title, message, activityContext.getString(android.R.string.yes), activityContext.getString(android.R.string.no), listener);
    }

    public void ask(Context activityContext, String title, String message, String positiveLabel, String negativeLabel, final AnswerListener listener) {

        DialogInterface.OnClickListener dialogListener = null;

        if (listener != null) {
            dialogListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE: {
                            listener.onPositive();
                            break;
                        }
                        case DialogInterface.BUTTON_NEGATIVE: {
                            listener.onNegative();
                            break;
                        }
                        default:
                            throw new IllegalArgumentException("Unsupported dialog result!");
                    }
                }
            };
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activityContext);
        builder.setMessage(message);

        if (title != null) {
            builder.setTitle(title);
        }

        if (positiveLabel != null) {
            builder.setPositiveButton(positiveLabel, dialogListener);
        }

        if (negativeLabel != null) {
            builder.setNegativeButton(negativeLabel, dialogListener);
        }

        builder.show();
    }

    public void choose(Context activityContext, @StringRes int title, String[] options, final ChoiceListener listener) {

        choose(activityContext, activityContext.getString(title), options, listener);
    }

    public void choose(Context activityContext, String title, String[] options, final ChoiceListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activityContext);

        if (title != null) {
            builder.setTitle(title);
        }

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (listener != null)
                    listener.onChoice(which);
            }
        });

        builder.show();
    }

    public void inform(Context activityContext, @StringRes int title, @StringRes int message) {

        inform(activityContext, activityContext.getString(title), activityContext.getString(message));
    }

    public void inform(Context activityContext, String title, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activityContext);
        builder.setMessage(message);

        if (title != null)
            builder.setTitle(title);

        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    public void pickDateTime(final Context context, final DateTime current, final IDateChosenListener dateChosenListener) {

        DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {

                TimePickerDialog timePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, final int hourOfDay, final int minute) {

                        DateTime pickedDate = new DateTime(year, monthOfYear + 1, dayOfMonth, hourOfDay, minute);

                        if (dateChosenListener != null)
                            dateChosenListener.dateChosen(pickedDate);
                    }
                }, current.getHourOfDay(), current.getMinuteOfHour(), true);

                timePicker.show();
            }
        }, current.getYear(), current.getMonthOfYear() - 1, current.getDayOfMonth());

        datePicker.show();
    }

    public void pickDate(final Context context, final DateTime current, final IDateChosenListener dateChosenListener) {

        DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {

                DateTime pickedDate = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);

                if (dateChosenListener != null)
                    dateChosenListener.dateChosen(pickedDate);
            }
        }, current.getYear(), current.getMonthOfYear() - 1, current.getDayOfMonth());

        datePicker.show();
    }
}
