package com.spooksoft.looker.services;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.spooksoft.looker.models.OrientationF;

/**
 * Created by wsura on 19.05.2017.
 */

public class SensorService {

    // Private fields ---------------------------------------------------------

    private final Context context;
    private final SensorManager sensorManager;
    private final Sensor accelerometer;
    private final Sensor magnetometer;

    private float[] lastAccel = new float[3];
    private float[] lastMagnet = new float[3];
    private boolean accelSet = false;
    private boolean magnetSet = false;

    private float[] r = new float[9];
    private float[] orientation = new float[3];

    private boolean collecting = false;

    // Private event listeners ------------------------------------------------

    private final SensorEventListener accelChanged = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            System.arraycopy(sensorEvent.values, 0, lastAccel, 0, sensorEvent.values.length);
            accelSet = true;

            evalOrientation();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private final SensorEventListener magnetChanged = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            System.arraycopy(sensorEvent.values, 0, lastMagnet, 0, sensorEvent.values.length);
            magnetSet = true;

            evalOrientation();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    // Private methods --------------------------------------------------------

    private void evalOrientation() {
        if (accelSet && magnetSet) {
            SensorManager.getRotationMatrix(r, null, lastAccel, lastMagnet);
            SensorManager.getOrientation(r, orientation);
        }
    }

    // Public methods ---------------------------------------------------------

    public SensorService(Context context) {
        this.context = context;

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void startCollecting() {

        sensorManager.registerListener(accelChanged, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(magnetChanged, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        collecting = true;
    }

    public void stopCollecting() {

        sensorManager.unregisterListener(accelChanged);
        sensorManager.unregisterListener(magnetChanged);
        collecting = false;

        accelSet = false;
        magnetSet = false;
    }

    public boolean isCollecting() {
        return collecting;
    }

    public OrientationF getOrientation() {

        if (collecting) {
            return new OrientationF(orientation[0], orientation[1], orientation[2]);
        }
        else
            throw new RuntimeException("Cannot return orientation when not collecting!");
    }
}
