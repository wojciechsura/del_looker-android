package com.spooksoft.looker.common;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by wsura on 26.05.2017.
 */

public class UDPClientThread extends HandlerThread implements INetClientThread {

    private static final String THREAD_NAME = "UDPClient";

    private static final int WM_SEND_STRING = 1;
    private static final int WM_SEND_BYTES = 2;

    private Handler handler = null;
    private DatagramSocket socket = null;
    private boolean isConnected = false;
    private final String address;
    private final int port;
    private final ConnectionResultListener connectionResultListener;

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();

        final InetAddress udpAddress;
        final int udpPort = port;

        try {
            socket = new DatagramSocket(port);
            udpAddress = InetAddress.getByName(address);
        } catch (Exception e) {

            if (socket != null && socket.isConnected()) {
                try {
                    socket.close();
                } catch (Exception e1) {

                }
            }
            socket = null;
            handler = null;

            isConnected = false;
            if (connectionResultListener != null)
                connectionResultListener.failure(e.getMessage());

            return;
        }

        handler = new Handler(getLooper()) {

            @Override
            public void handleMessage(Message msg) {

                if (msg.what == WM_SEND_STRING && msg.obj instanceof String) {
                    String message = (String) msg.obj;
                    try {

                        byte[] messageBytes = message.getBytes();
                        DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length, udpAddress, port);
                        socket.send(packet);
                    } catch (IOException e) {
                        Log.e("UDP client thread", "Cannot send message!");
                    }
                } else if (msg.what == WM_SEND_BYTES && msg.obj instanceof char[]) {

                    char[] data = (char[]) msg.obj;
                    try {
                        byte[] byteData = new byte[data.length];
                        for (int i = 0; i < data.length; i++)
                            byteData[i] = (byte) data[i];
                        DatagramPacket packet = new DatagramPacket(byteData, data.length);
                        socket.send(packet);
                    } catch (IOException e) {
                        Log.e("TCP client thread", "Cannot send message!");
                    }
                }
            }
        };

        isConnected = true;
        connectionResultListener.success();
    }

    public UDPClientThread(String address, int port, ConnectionResultListener connectionResultListener) {
        super(THREAD_NAME);

        this.address = address;
        this.port = port;
        this.connectionResultListener = connectionResultListener;
    }

    public void send(String message) {

        if (!isConnected)
            throw new RuntimeException("Cannot send data, not connected!");

        Message msg = new Message();
        msg.obj = message;
        msg.what = WM_SEND_STRING;
        handler.sendMessage(msg);
    }

    public void send(char[] data) {

        if (!isConnected)
            throw new RuntimeException("Cannot send data, not connected!");

        Message msg = new Message();
        msg.obj = data;
        msg.what = WM_SEND_BYTES;
        handler.sendMessage(msg);
    }

    public void close() {

        if (!isConnected)
            throw new RuntimeException("Cannot close, not connected!");

        socket.close();
    }
}
