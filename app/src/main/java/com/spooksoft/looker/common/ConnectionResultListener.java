package com.spooksoft.looker.common;

/**
 * Created by wsura on 26.05.2017.
 */

public interface ConnectionResultListener {

    void success();
    void failure(String reason);
}