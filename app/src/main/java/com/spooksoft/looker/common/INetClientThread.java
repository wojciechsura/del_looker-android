package com.spooksoft.looker.common;

/**
 * Created by wsura on 26.05.2017.
 */

public interface INetClientThread {
    void start();
    void close();
    boolean quit();
    void send(String data);
    void send(char[] data);
}
