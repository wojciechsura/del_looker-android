package com.spooksoft.looker.models;

/**
 * Created by wsura on 22.05.2017.
 */

public class Orientation {
    private final int yaw;
    private final int pitch;
    private final int roll;

    public Orientation(int yaw, int pitch, int roll) {
        this.yaw = yaw;
        this.pitch = pitch;
        this.roll = roll;
    }

    public int getYaw() {
        return yaw;
    }

    public int getPitch() {
        return pitch;
    }

    public int getRoll() {
        return roll;
    }
}
