package com.spooksoft.looker.models;

/**
 * Created by wsura on 19.05.2017.
 */

public class OrientationF {
    private final float yaw;
    private final float pitch;
    private final float roll;

    public OrientationF(float yaw, float pitch, float roll) {
        this.yaw = yaw;
        this.pitch = pitch;
        this.roll = roll;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public float getRoll() {
        return roll;
    }
}
